# CS3200-PROJECT
This is for the java file that we will be modifying

## StockMarket Trade Game ##

StockMarket trading game is capable of updating the information on the price of stocks. To do this you must be logged in as an admin and use the update commands.

To start playing, as an admin, you must first define a firm with traders in it. After creating a firm, you must select a trader. When you select a valid trader
in the database, you can buy, sell stocks, or check standings.

### Commands ###

After any prompt by the program, enter help for a list of commands.

### Needed procedures ###

We still need a procedure that given a stock, will decrement the amount of that stock in the inventory of the trader and increment that traders $$$ field by the
appropriate amount at the given date.
I am planning on using a counted for loop to call this procedure the specified number of times.

We still need a procedure that given a stock, will increment the amount of that stock in the inventory of the trader and decrement that traders $$$ field by the
appropriate amount at the given date.
I am planning on using a counted for loop to call this procedure the specified amount of times.

### Needed triggers ###

Unknown at this time.
